// ** React Import
import { useState } from "react";

// ** Axios Imports
import axios from "axios";
axios.defaults.withCredentials = true;

// ** Custom Components
import AsyncSelect from "react-select/async";
import Sidebar from "@components/sidebar";

// ** Third Party Components

// ** Reactstrap Imports
import { Button, Label, Form, Input, FormFeedback, Spinner } from "reactstrap";

import Flatpickr from 'react-flatpickr';

// ** Styles
import "@styles/react/libs/flatpickr/flatpickr.scss";

// ** Utils
import { selectThemeColors } from "@utils";

// ** Store & Actions
import { createBooking, updateBooking } from "@store/bookings";
import { useDispatch } from "react-redux";

const BookingSidebar = ({ open, toggleSidebar, refreshTable, booking = null }) => {
  // ** States
  const [name, setName] = useState(() => {
    return booking ? booking.name : "";
  });

  const [ownerTimezone, setOwnerTimezone] = useState(() => {
    if (booking && booking.owner_timezone) {
      return {
        label: booking.owner_timezone.name,
        value: booking.owner_timezone.id,
        id: booking.owner_timezone.id,
      };
    } else {
      return null;
    }
  });

  const [recipientTimezone, setRecipientTimezone] = useState(() => {
    if (booking && booking.recipient_timezone) {
      return {
        label: booking.recipient_timezone.name,
        value: booking.recipient_timezone.id,
        id: booking.recipient_timezone.id,
      };
    } else {
      return null;
    }
  });

  const [startTime, setStartTime] = useState(() => {
    return booking ? booking.start_time : null;
  });

  const [endTime, setEndTime] = useState(() => {
    return booking ? booking.end_time : null;
  });

  const [ownerQuery, setOwnerQuery] = useState("");
  const loadOwnerTimezonesOption = async () => {
    const res = await axios.get(
      `${process.env.REACT_APP_API_ENDPOINT}/api/timezones?q=${ownerQuery}`
    );
    const timezones = res.data.map((timezone) => {
      return {
        id: timezone.id,
        value: timezone.id,
        label: timezone.name,
      };
    });
    return timezones;
  };

  const handleOwnerTimezoneInputChange = (newValue) => {
    setOwnerQuery(newValue);
  };

  const [recipientQuery, setRecipientQuery] = useState("");
  const loadRecipientTimezonesOption = async () => {
    const res = await axios.get(
      `${process.env.REACT_APP_API_ENDPOINT}/api/timezones?q=${recipientQuery}`
    );
    const timezones = res.data.map((timezone) => {
      return {
        id: timezone.id,
        value: timezone.id,
        label: timezone.name,
      };
    });
    return timezones;
  };

  const handleRecipientTimezoneInputChange = (newValue) => {
    setRecipientQuery(newValue);
  };

  const [nameError, setNameError]                           = useState(false);
  const [ownerTimezoneError, setOwnerTimezoneError]         = useState(false);
  const [recipientTimezoneError, setRecipientTimezoneError] = useState(false);
  const [startTimeError, setStartTimeError]                 = useState(false);
  const [endTimeError, setEndTimeError]                     = useState(false);
  const [formSubmissionLoader, setFormSubmissionLoader]     = useState(false);

  // ** Store Vars
  const dispatch = useDispatch();

  // ** Function to handle form submit
  const onSubmit = (e) => {
    e.preventDefault();

    console.log(startTime);

    let valid = true;

    if (!name) {
      valid = false;
      setNameError(true);
    } else {
      setNameError(false);
    }

    if (!ownerTimezone) {
      valid = false;
      setOwnerTimezoneError(true);
    } else {
      setOwnerTimezoneError(false);
    }

    if (!recipientTimezone) {
      valid = false;
      setRecipientTimezoneError(true);
    } else {
      setRecipientTimezoneError(false);
    }

    if (!startTime || startTime.length === 0) {
      valid = false;
      setStartTimeError(true);
    } else {
      setStartTimeError(false);
    }

    if (!endTime || endTime.length === 0) {
      valid = false;
      setEndTimeError(true);
    } else {
      setEndTimeError(false);
    }

    if (valid) {
      setFormSubmissionLoader(true);

      if (booking) {
        dispatch(updateBooking({ name, owner_timezone_id: ownerTimezone.id, recipient_timezone_id: recipientTimezone.id, start_time: startTime, end_time: endTime, id: booking.id 
        })).then(
          (result) => {
            setFormSubmissionLoader(false);
            if (result.payload.data.booking) {
              setName("");
              setOwnerTimezone("");
              setRecipientTimezone("");
              setStartTime("");
              setEndTime("");
              refreshTable();
              toggleSidebar();
            }
          }
        );
      } else {
        dispatch(createBooking({ name, owner_timezone_id: ownerTimezone.id, recipient_timezone_id: recipientTimezone.id, start_time: startTime, end_time: endTime }))
        .then((result) => {
          setFormSubmissionLoader(false);
          if (result.payload.data.booking) {
            setName("");
            setOwnerTimezone("");
            setRecipientTimezone("");
            setStartTime("");
            setEndTime("");
            refreshTable();
            toggleSidebar();
          }
        });
      }
    }
  };

  const handleSidebarClosed = () => {
    setName("");
    setOwnerTimezone("");
    setRecipientTimezone("");
    setStartTime("");
    setEndTime("");

    setNameError(false);
    setOwnerTimezoneError(false);
    setRecipientTimezoneError(false);
    setStartTimeError(false);
    setEndTimeError(false);
  };

  return (
    <Sidebar size="lg" open={open} title={booking ? "Edit Booking" : "New Booking"} headerClassName="mb-1" contentClassName="pt-0"
      toggleSidebar={toggleSidebar} onClosed={handleSidebarClosed}>
      <Form>
        <div className="mb-1">
          <Label className="form-label" for="name">
            Name <span className="text-danger">*</span>
          </Label>

          <Input id="name" placeholder="Booking 1" invalid={nameError} value={name} onChange={(e) => setName(e.target.value)}/>
          <FormFeedback>Please enter a valid name</FormFeedback>
        </div>

        <div className="mb-1">
          <Label className="form-label" for="ownerTimezoneInput">
            Owner Timezone
          </Label>
          <AsyncSelect
            defaultOptions
            isClearable={false}
            value={ownerTimezone}
            name="ownerTimezone"
            className="react-select"
            id="ownerTimezoneInput"
            classNamePrefix="select"
            onChange={(timezone) => {
              setOwnerTimezone(timezone);
            }}
            theme={selectThemeColors}
            loadOptions={loadOwnerTimezonesOption}
            onInputChange={handleOwnerTimezoneInputChange}
            noOptionsMessage={(input) => {
              return `No match found for ${input.inputValue}!`;
            }}
          />

          {ownerTimezoneError && (
            <div
              className="invalid-feedback"
              style={{ display: "block" }}
            >
              Please select timezone
            </div>
          )}
        </div>

        <div className="mb-1">
          <Label className="form-label" for="ownerTimezoneInput">
            Recipient Timezone
          </Label>
          <AsyncSelect
            defaultOptions
            isClearable={false}
            value={recipientTimezone}
            name="ownerTimezone"
            className="react-select"
            id="ownerTimezoneInput"
            classNamePrefix="select"
            onChange={(timezone) => {
              setRecipientTimezone(timezone);
            }}
            theme={selectThemeColors}
            loadOptions={loadRecipientTimezonesOption}
            onInputChange={handleRecipientTimezoneInputChange}
            noOptionsMessage={(input) => {
              return `No match found for ${input.inputValue}!`;
            }}
          />

          {recipientTimezoneError && (
            <div
              className="invalid-feedback"
              style={{ display: "block" }}
            >
              Please select timezone
            </div>
          )}
        </div>

        <div className="mb-1">
          <Label className='form-label' for='start-datetime'>
            Start Time
          </Label>
          <Flatpickr
            value={startTime}
            data-enable-time
            id='start-datetime'
            className='form-control'
            onChange={(selectedDates, dateStr) => {
              setStartTime(dateStr);
            }}
            // options={{
            //   // altInput: true,
            //   // altFormat: 'F j, Y',
            //   dateFormat: 'Y-m-d H:i'
            // }}
          />

          {startTimeError && (
            <div
              className="invalid-feedback"
              style={{ display: "block" }}
            >
              Please select start time
            </div>
          )}
        </div>

        <div className="mb-1">
          <Label className='form-label' for='end-datetime'>
            End Time
          </Label>
          <Flatpickr
            value={endTime}
            data-enable-time
            id='end-datetime'
            className='form-control'
            onChange={(selectedDates, dateStr) => {
              setEndTime(dateStr);
            }}
          />

          {endTimeError && (
            <div
              className="invalid-feedback"
              style={{ display: "block" }}
            >
              Please select end time
            </div>
          )}
        </div>

        <Button onClick={(e) => onSubmit(e)} className="me-1" color="primary">
          Submit {formSubmissionLoader && (
            <Spinner style={{ marginLeft: "5px" }} size={"sm"} color="white" />
          )}
        </Button>
        <Button type="reset" color="secondary" outline onClick={toggleSidebar}>
          Cancel
        </Button>
      </Form>
    </Sidebar>
  );
};

export default BookingSidebar;
