// ** React Imports
import { Link } from "react-router-dom";

// ** Custom Components
import Avatar from "@components/avatar";

// ** Icons Imports
import { Slack, User, Settings, Database, Edit2, MoreVertical, FileText, Trash2, Archive } from "react-feather";

// ** Reactstrap Imports
import { Badge, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Input, Label } from "reactstrap";

const renderCreatedAt = (row) => {
  return row.createdAt ? row.createdAt : "-";
};

const Title = () => (
  <div style={{ paddingLeft: "25px" }} className="d-flex justify-content-center align-items-center">
    Name
  </div>
);
const OwnerTimezone = () => (
  <div className="d-flex justify-content-center align-items-center">
    Owner Timezone
  </div>
);
const RecipientTimezone = () => (
  <div className="d-flex justify-content-center align-items-center">
    Recipient Timezone
  </div>
);
export const bookingTableColumns = [
  {
    name: <Title />,
    sortable: false,
    minWidth: "200px",
    selector: (row) => row.name,
    cell: (row) => row.name,
  },
  {
    name: <OwnerTimezone />,
    sortable: false,
    minWidth: "200px",
    selector: (row) => row.owner_timezone.name,
    cell: (row) => row.owner_timezone.name,
  },
  {
    name: <RecipientTimezone />,
    sortable: false,
    minWidth: "200px",
    selector: (row) => row.recipient_timezone.name,
    cell: (row) => row.recipient_timezone.name,
  },
  {
    name: "Start time owner",
    sortable: false,
    minWidth: "182px",
    selector: (row) => row.start_time_owner,
    cell: (row) => row.start_time_owner,
  },
  {
    name: "End time owner",
    sortable: false,
    minWidth: "182px",
    selector: (row) => row.end_time_owner,
    cell: (row) => row.end_time_owner,
  },
  {
    name: "Start time recipient",
    sortable: false,
    minWidth: "182px",
    selector: (row) => row.start_time_recipient,
    cell: (row) => row.start_time_recipient,
  },
  {
    name: "End time recipient",
    sortable: false,
    minWidth: "182px",
    selector: (row) => row.end_time_recipient,
    cell: (row) => row.end_time_recipient,
  },
  {
    name: "Created At",
    sortable: true,
    minWidth: "182px",
    sortField: "createdAt",
    selector: (row) => row.createdAt,
    cell: (row) => renderCreatedAt(row),
  },
  {
    name: "Actions",
    minWidth: "100px",
    cell: (row) => (
      <div className="column-action">
        <UncontrolledDropdown>
          <DropdownToggle tag="div" className="btn btn-sm">
            <MoreVertical size={14} className="cursor-pointer" />
          </DropdownToggle>
          <DropdownMenu>
             <DropdownItem tag="a" className="w-100" onClick={(e) => { row.handleEdit(row.id); e.preventDefault(); }}>
              <Archive size={14} className="me-50" />
              <span className="align-middle">Edit</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </div>
    ),
  },
];