import Table from "./Table";

const Bookings = () => {
  return (
    <div className="app-user-list">
      <Table />
    </div>
  );
};

export default Bookings;
