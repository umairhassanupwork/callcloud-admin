// ** React Imports
import React, { Fragment, useEffect, useState } from "react";

// ** Invoice List Sidebar
import Sidebar from "./Sidebar";

// ** Table Columns
import { bookingTableColumns } from "./columns";

// ** Confirm box
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import "@styles/base/plugins/extensions/ext-component-sweet-alerts.scss";
const MySwal = withReactContent(Swal);

// ** Store & Actions
import { getBookings, storeCurrentPage, storeRowsPerPage } from "@store/bookings";
import { useDispatch, useSelector } from "react-redux";

// ** Third Party Components
import ReactPaginate from "react-paginate";
import DataTable from "react-data-table-component";
import { ChevronDown } from "react-feather";

// ** Utils
// eslint-disable-next-line no-unused-vars
import { selectThemeColors } from "@utils";

// ** Reactstrap Imports
import { Row, Col, Card, Input, Button } from "reactstrap";

// ** Styles
import "@styles/react/libs/react-select/_react-select.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";

// ** Table Header
const CustomHeader = ({ toggleSidebar, handlePerPage, rowsPerPage, handleFilter, searchTerm, setEditBooking }) => {
   return (
      <div className="invoice-list-table-header w-100 me-1 ms-50 mt-2 mb-75">
         <Row>
            <Col xl="6" className="d-flex align-items-center p-0">
               <div className="d-flex align-items-center mb-sm-0 mb-1 me-1">
                  <label className="mb-0" htmlFor="search-invoice"></label>
                  <Input id="search-invoice" className="ms-50 w-100" type="text" value={searchTerm} placeholder="Type to find" onChange={(e) => handleFilter(e.target.value)}/>
               </div>
            </Col>

            <Col xl="6" className="d-flex align-items-sm-center justify-content-xl-end justify-content-start flex-xl-nowrap flex-wrap flex-sm-row flex-column pe-xl-1 p-0 mt-xl-0 mt-1">
               <div className="d-flex align-items-center mx-1">
                  <label htmlFor="rows-per-page">Show</label>
                  <Input className="mx-50" type="select" id="rows-per-page" value={rowsPerPage} onChange={handlePerPage} style={{ width: "5rem" }}>
                     <option value="10">10</option>
                     <option value="25">25</option>
                     <option value="50">50</option>
                  </Input>
               </div>

               <div className="d-flex align-items-center table-header-actions">
                  <Button className="add-new-user" color="primary" onClick={() => { setEditBooking(null); toggleSidebar(); }}>Add New Booking
                  </Button>
               </div>
            </Col>
         </Row>
      </div>
   );
};

const BookingsList = () => {
   // ** Store Vars
   const dispatch = useDispatch();
   const store    = useSelector((state) => state.bookings);

   // ** States
   const [sort, setSort]               = useState("desc");
   const [searchTerm, setSearchTerm]   = useState("");
   const [currentPage, setCurrentPage] = useState(() => store.currentPage);
   const [sortColumn, setSortColumn]   = useState("");
   const [rowsPerPage, setRowsPerPage] = useState(() => store.rowsPerPage);
   const [sidebarOpen, setSidebarOpen] = useState(false);

   const [editBooking, setEditBooking] = useState(null);

   // ** Function to toggle sidebar
   const toggleSidebar = () => {
      setSidebarOpen(!sidebarOpen);
   };

   const refreshTable = () => {
      dispatch(
         getBookings({
            sort,
            sortColumn,
            q: searchTerm,
            page: currentPage,
            perPage: rowsPerPage,
         })
      );
   };

   // ** Get data on mount
   useEffect(() => {
      dispatch(
         getBookings({
            sort,
            sortColumn,
            q: searchTerm,
            page: currentPage,
            perPage: rowsPerPage,
         })
      );
   }, []);

   // ** Function in get data on page change
   const handlePagination = (page) => {
      dispatch(
         getBookings({
            sort,
            sortColumn,
            q: searchTerm,
            perPage: rowsPerPage,
            page: page.selected + 1,
         })
      );
      const newPage = page.selected + 1;
      setCurrentPage(newPage);
      dispatch(storeCurrentPage({ currentPage: newPage }));
   };

   // ** Function in get data on rows per page
   const handlePerPage = (e) => {
      const value = parseInt(e.currentTarget.value);
      dispatch(
         getBookings({
            sort,
            sortColumn,
            q: searchTerm,
            perPage: value,
            page: 1,
         })
      );
      setCurrentPage(1);
      setRowsPerPage(value);
      dispatch(storeCurrentPage({ currentPage: 1 }));
      dispatch(storeRowsPerPage({ rowsPerPage: value }));
   };

   // ** Function in get data on search query change
   const handleFilter = (val) => {
      let page = currentPage;
      if (val && !searchTerm) {
         page = 1;
         setCurrentPage(1);
      }

      setSearchTerm(val);
      dispatch(
         getBookings({
            sort,
            q: val,
            sortColumn,
            page,
            perPage: rowsPerPage,
         })
      );
   };

   // ** Custom Pagination
   const CustomPagination = () => {
      const count = Number(Math.ceil(store.totalBookings / rowsPerPage));

      return (
         <ReactPaginate previousLabel={""} nextLabel={""} pageCount={count || 1} activeClassName="active" forcePage={currentPage !== 0 ? currentPage - 1 : 0} onPageChange={(page) => handlePagination(page)} pageClassName={"page-item"} nextLinkClassName={"page-link"} nextClassName={"page-item next"} previousClassName={"page-item prev"} previousLinkClassName={"page-link"} pageLinkClassName={"page-link"}
         containerClassName={ "pagination react-paginate justify-content-end my-2 pe-1" } />
      );
   };

   // ** Table data to render
   const dataToRender = () => {
      const filters = {
         q: searchTerm,
      };

      const isFiltered = Object.keys(filters).some(function (k) {
         return filters[k].length > 0;
      });

      if (store.bookings.length > 0) {
         const bookings = store.bookings.map((booking) => {
            const tempBooking = { ...booking };
            tempBooking.handleEdit = (id) => {
               const editBooking = store.bookings.filter((booking) => booking.id === id);
               if (editBooking.length) {
                  setEditBooking(editBooking[0]);
                  toggleSidebar();
               }
            };
            return tempBooking;
         });
         return bookings;
      } else if (store.bookings.length === 0 && isFiltered) {
         return [];
      } else {
         return store.bookings.slice(0, rowsPerPage);
      }
   };

   const handleSort = (column, sortDirection) => {
      setSort(sortDirection);
      setSortColumn(column.sortField);
      dispatch(
         getBookings({
            sort: sortDirection,
            sortColumn: column.sortField,
            q: searchTerm,
            page: currentPage,
            perPage: rowsPerPage,
         })
      );
   };

   return (
      <Fragment>
         <Card className="overflow-hidden workspace-list">
            <div className="react-dataTable">
               <DataTable noHeader subHeader sortServer pagination responsive paginationServer columns={bookingTableColumns} onSort={handleSort} sortIcon={<ChevronDown />} className="react-dataTable" paginationComponent={CustomPagination} data={dataToRender()}
                  subHeaderComponent={ 
                     <CustomHeader store={store} searchTerm={searchTerm} rowsPerPage={rowsPerPage} handleFilter={handleFilter} handlePerPage={handlePerPage} toggleSidebar={toggleSidebar} setEditBooking={setEditBooking}/>
                  }
               />
            </div>
         </Card>

         {sidebarOpen && (
            <Sidebar open={sidebarOpen} refreshTable={refreshTable} toggleSidebar={toggleSidebar} booking={editBooking}/>
         )}
      </Fragment>
   );
};

export default BookingsList;
