// ** React Import
import { useState } from "react";

// ** Custom Components
import Sidebar from "@components/sidebar";

// ** Utils
// import { selectThemeColors } from "@utils";

// ** Third Party Components

// ** Reactstrap Imports
import { Button, Label, Form, Input, FormFeedback, Spinner } from "reactstrap";
import classnames from "classnames";

// ** Store & Actions
import { createPlan, updatePlan, getActivePlans } from "@store/plans";
import { useDispatch } from "react-redux";

const PlanSidebar = ({ open, toggleSidebar, refreshTable, plan = null }) => {
  // ** States

  const [name, setName] = useState(() => {
    return plan ? plan.name : "";
  });

  const [description, setDescription] = useState(() => {
    return plan && plan.description ? plan.description : "";
  });

  const [nameError, setNameError] = useState(false);
  const [descriptionError, setDescriptionError] = useState(false);
  const [formSubmissionLoader, setFormSubmissionLoader] = useState(false);
  const [isFreePlan, setIsFreePlan] = useState(false);
  const [trialDays, setTrialDays] = useState(30);

  // const [plan, setPlan] = useState("basic");
  // const [role, setRole] = useState("subscriber");

  // ** Store Vars
  const dispatch = useDispatch();

  // ** Function to handle form submit
  const onSubmit = (e) => {
    e.preventDefault();

    let valid = true;

    if (!name) {
      valid = false;
      setNameError(true);
    } else {
      setNameError(false);
    }

    if (!description) {
      valid = false;
      setDescriptionError(true);
    } else {
      setDescriptionError(false);
    }

    if (valid) {
      setFormSubmissionLoader(true);

      if (plan) {
        dispatch(updatePlan({ name, description, id: plan.id })).then(
          (result) => {
            setFormSubmissionLoader(false);
            if (result.payload.data.plan) {
              setName("");
              setDescription("");
              refreshTable();
              toggleSidebar();
              dispatch(getActivePlans());
            }
          }
        );
      } else {
        dispatch(createPlan({ name, description, isFreePlan, trialDays })).then(
          (result) => {
            setFormSubmissionLoader(false);
            if (result.payload.data.plan) {
              setName("");
              setDescription("");
              setIsFreePlan(false);
              setTrialDays(30);
              refreshTable();
              toggleSidebar();
            }
          }
        );
      }
    }
  };

  const handleSidebarClosed = () => {
    setName("");
    setDescription("");
    setNameError(false);
    setDescriptionError(false);
  };

  return (
    <Sidebar
      size="lg"
      open={open}
      title={plan ? "Edit Plan" : "New Plan"}
      headerClassName="mb-1"
      contentClassName="pt-0"
      toggleSidebar={toggleSidebar}
      onClosed={handleSidebarClosed}
    >
      <Form>
        <div className="mb-1">
          <Label className="form-label" for="name">
            Name <span className="text-danger">*</span>
          </Label>

          <Input
            id="name"
            placeholder="Standard"
            invalid={nameError}
            value={name}
            // disabled={pl}
            onChange={(e) => setName(e.target.value)}
          />

          <FormFeedback>Please enter a valid Name</FormFeedback>
        </div>

        <div className="form-floating my-2">
          <Input
            name="description"
            value={description}
            type="textarea"
            id="description"
            placeholder="Description about the plan"
            style={{ minHeight: "100px" }}
            onChange={(e) => setDescription(e.target.value)}
            invalid={descriptionError}
            className={classnames({ "text-danger": description.length > 200 })}
          />
          <Label className="form-label" for="description">
            Description
          </Label>
          <span
            className={classnames("textarea-counter-value float-end", {
              "bg-danger": description.length > 200,
            })}
          >
            {`${description.length}/200`}
          </span>
        </div>

        {!plan && (
          <div className="form-check mb-1">
            <Input
              value={isFreePlan}
              type="checkbox"
              onChange={(e) => {
                setIsFreePlan(e.target.checked);
              }}
              id="is_free_plan"
            />
            <Label className="form-check-label" for="is_free_plan">
              Is free plan?
            </Label>
          </div>
        )}

        {isFreePlan && (
          <div className="mb-1">
            <Label className="form-check-label" for="trial_days">
              Expire after (Days)
            </Label>
            <Input
              list="trial_days_options"
              value={trialDays}
              type="number"
              onChange={(e) => {
                setTrialDays(e.target.value);
              }}
              id="trial_days"
            />
            <datalist id="trial_days_options">
              <option value={30} label="30 Days" />
              <option value={60} label="2 Months" />
              <option value={90} label="3 Months" />
              <option value={120} label="4 Months" />
              <option value={180} label="6 Months" />
              <option value={270} label="9 Months" />
              <option value={365} label="1 Year" />
            </datalist>
          </div>
        )}

        <Button onClick={(e) => onSubmit(e)} className="me-1" color="primary">
          Submit
          {formSubmissionLoader && (
            <Spinner style={{ marginLeft: "5px" }} size={"sm"} color="white" />
          )}
        </Button>
        <Button type="reset" color="secondary" outline onClick={toggleSidebar}>
          Cancel
        </Button>
      </Form>
    </Sidebar>
  );
};

export default PlanSidebar;
