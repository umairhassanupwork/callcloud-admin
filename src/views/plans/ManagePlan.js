import { useParams } from "react-router-dom";
import { Fragment } from "react";
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Input,
  Label,
  FormFeedback,
} from "reactstrap";

import PriceTable from "./PriceTable";
import FeatureList from "./FeatureList";

// import { getPrices } from "@store/plans";
// import { useDispatch } from "react-redux";

const ManagePlan = () => {
  // const dispatch = useDispatch();
  // const [prices, setPrices] = useState([]);
  const params = useParams();

  // useEffect(() => {
  //   dispatch(getPrices({ id: params.id }))
  //     .unwrap()
  //     .then((d) => {
  //       console.log(d);
  //       setPrices(d.data.prices);
  //     });
  // }, []);
  return (
    <Fragment>
      <Card>
        <CardHeader className="border-bottom">
          <CardTitle tag="h4">Manage Prices</CardTitle>
        </CardHeader>
        <CardBody className="py-2 my-25">
          <div className="app-user-list">
            <PriceTable planId={params.id} />
          </div>
        </CardBody>
      </Card>

      <Card>
        <CardHeader className="border-bottom">
          <CardTitle tag="h4">Manage Features</CardTitle>
        </CardHeader>
        <CardBody className="py-2 my-25">
          <FeatureList planId={params.id} />
        </CardBody>
      </Card>
    </Fragment>
  );
};

export default ManagePlan;
