// ** React Imports
import React, { Fragment, useEffect, useState } from "react";
// import { useLocation } from "react-router-dom";

// ** Invoice List Sidebar
import PriceSidebar from "./PriceSidebar";

// ** Table Columns
import { priceTableColumns } from "./columns";

// ** Confirm box
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import "@styles/base/plugins/extensions/ext-component-sweet-alerts.scss";
const MySwal = withReactContent(Swal);

// ** Store & Actions
// import { getAllData, getData } from "../store";
import {
  getPrices,
  storeCurrentPagePrice,
  storeRowsPerPagePrice,
  updatePrice,
  // deleteWorkspace,
} from "@store/plans";
import { useDispatch, useSelector } from "react-redux";

// ** Third Party Components
import ReactPaginate from "react-paginate";
import DataTable from "react-data-table-component";
import { ChevronDown } from "react-feather";

// ** Utils
// eslint-disable-next-line no-unused-vars
import { selectThemeColors } from "@utils";

// ** Reactstrap Imports
import { Row, Col, Card, Input, Button } from "reactstrap";

// ** Styles
import "@styles/react/libs/react-select/_react-select.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";

// ** Table Header
const CustomHeader = ({
  //   store,
  toggleSidebar,
  handlePerPage,
  rowsPerPage,
  store,
  //   handleFilter,
  //   searchTerm,
}) => {
  return (
    <div className="invoice-list-table-header w-100 me-1 ms-50 mt-2 mb-75">
      <Row>
        {/* <Col xl="6" className="d-flex align-items-center p-0">
          <div className="d-flex align-items-center mb-sm-0 mb-1 me-1">
            <label className="mb-0" htmlFor="search-invoice"></label>
            <Input
              id="search-invoice"
              className="ms-50 w-100"
              type="text"
              value={searchTerm}
              placeholder="Type to find"
              onChange={(e) => handleFilter(e.target.value)}
            />
          </div>
        </Col> */}

        <Col
          //   xl="6"
          className="d-flex align-items-sm-center justify-content-xl-end justify-content-start flex-xl-nowrap flex-wrap flex-sm-row flex-column pe-xl-1 p-0 mt-xl-0 mt-1"
        >
          <div className="d-flex align-items-center mx-1">
            <label htmlFor="rows-per-page">Show</label>
            <Input
              className="mx-50"
              type="select"
              id="rows-per-page"
              value={rowsPerPage}
              onChange={handlePerPage}
              style={{ width: "5rem" }}
            >
              <option value="10">10</option>
              <option value="25">25</option>
              <option value="50">50</option>
            </Input>
          </div>

          <div className="d-flex align-items-center flex-column table-header-actions">
            <Button
              disabled={store.isFreePlan}
              className="add-new-user"
              color="primary"
              onClick={() => {
                // setEditPlan(null);
                toggleSidebar();
              }}
            >
              Add New Price
            </Button>
            {store.isFreePlan && (
              <p className="d-block invalid-feedback">
                You can not create price of free plan!
              </p>
            )}
          </div>
        </Col>
      </Row>
    </div>
  );
};

const PricesList = ({ planId }) => {
  // ** Store Vars
  const dispatch = useDispatch();
  const store = useSelector((state) => state.plans);
  // const userData = useSelector((state) => state.auth);

  // ** States
  const [sort, setSort] = useState("desc");
  const [searchTerm, setSearchTerm] = useState("");
  const [currentPage, setCurrentPage] = useState(() => store.currentPagePrice);
  const [sortColumn, setSortColumn] = useState("createdAt");
  const [rowsPerPage, setRowsPerPage] = useState(() => store.rowsPerPagePrice);
  const [sidebarOpen, setSidebarOpen] = useState(false);

  // ** Function to toggle sidebar
  const toggleSidebar = () => {
    setSidebarOpen(!sidebarOpen);
  };

  const refreshTable = () => {
    dispatch(
      getPrices({
        sort,
        sortColumn,
        q: searchTerm,
        page: currentPage,
        perPage: rowsPerPage,
        id: planId,
      })
    );
  };

  // ** Get data on mount
  useEffect(() => {
    dispatch(
      getPrices({
        sort,
        sortColumn,
        q: searchTerm,
        page: currentPage,
        perPage: rowsPerPage,
        id: planId,
      })
    );
  }, []);

  // ** Function in get data on page change
  const handlePagination = (page) => {
    dispatch(
      getPrices({
        sort,
        sortColumn,
        q: searchTerm,
        perPage: rowsPerPage,
        page: page.selected + 1,
        id: planId,
      })
    );
    const newPage = page.selected + 1;
    setCurrentPage(newPage);
    dispatch(storeCurrentPagePrice({ currentPage: newPage }));
  };

  // ** Function in get data on rows per page
  const handlePerPage = (e) => {
    const value = parseInt(e.currentTarget.value);
    dispatch(
      getPrices({
        sort,
        sortColumn,
        q: searchTerm,
        perPage: value,
        page: 1,
        id: planId,
      })
    );
    setCurrentPage(1);
    setRowsPerPage(value);
    dispatch(storeCurrentPagePrice({ currentPage: 1 }));
    dispatch(storeRowsPerPagePrice({ rowsPerPage: value }));
  };

  // ** Function in get data on search query change
  const handleFilter = (val) => {
    let page = currentPage;
    if (val && !searchTerm) {
      page = 1;
      setCurrentPage(1);
    }

    setSearchTerm(val);
    dispatch(
      getPrices({
        sort,
        q: val,
        sortColumn,
        page,
        perPage: rowsPerPage,
        id: planId,
      })
    );
  };

  // ** Custom Pagination
  const CustomPagination = () => {
    const count = Number(Math.ceil(store.total / rowsPerPage));

    return (
      <ReactPaginate
        previousLabel={""}
        nextLabel={""}
        pageCount={count || 1}
        activeClassName="active"
        forcePage={currentPage !== 0 ? currentPage - 1 : 0}
        onPageChange={(page) => handlePagination(page)}
        pageClassName={"page-item"}
        nextLinkClassName={"page-link"}
        nextClassName={"page-item next"}
        previousClassName={"page-item prev"}
        previousLinkClassName={"page-link"}
        pageLinkClassName={"page-link"}
        containerClassName={
          "pagination react-paginate justify-content-end my-2 pe-1"
        }
      />
    );
  };

  // ** Table data to render
  const dataToRender = () => {
    const filters = {
      q: searchTerm,
    };

    const isFiltered = Object.keys(filters).some(function (k) {
      return filters[k].length > 0;
    });

    if (store.prices.length > 0) {
      const prices = store.prices.map((price) => {
        const tempPrice = { ...price };

        tempPrice.handleStatusChange = async (id, status) => {
          console.log(id, status);

          // prettier-ignore
          const text = status ? "Are you sure you want to activate this price?" : "Are you sure you want to deactivate this price?";
          // prettier-ignore
          const confirmButtonText = status ? "Yes, activate this price" : "Yes, deactivate this price";
          const result = await MySwal.fire({
            title: "Are you sure?",
            text,
            icon: "warning",
            showCancelButton: true,
            confirmButtonText,
            customClass: {
              confirmButton: "btn btn-primary",
              cancelButton: "btn btn-danger ms-1",
            },
            buttonsStyling: false,
          });

          if (result.value) {
            dispatch(updatePrice({ id, active: status })).then((result) => {
              if (result.payload.data.price) {
                refreshTable();
              }
            });
          }
        };
        return tempPrice;
      });

      return prices;
    } else if (store.prices.length === 0 && isFiltered) {
      return [];
    } else {
      return store.prices.slice(0, rowsPerPage);
    }
  };

  const handleSort = (column, sortDirection) => {
    console.log(column, sortDirection);
    setSort(sortDirection);
    setSortColumn(column.sortField);
    dispatch(
      getPrices({
        sort: sortDirection,
        sortColumn: column.sortField,
        q: searchTerm,
        page: currentPage,
        perPage: rowsPerPage,
        id: planId,
      })
    );
  };

  return (
    <Fragment>
      <Card className="overflow-hidden workspace-list">
        {!store.isFreePlan ? (
          <div className="react-dataTable">
            <DataTable
              noHeader
              subHeader
              sortServer
              pagination
              responsive
              paginationServer
              // prettier-ignore
              columns={priceTableColumns}
              onSort={handleSort}
              sortIcon={<ChevronDown />}
              className="react-dataTable"
              paginationComponent={CustomPagination}
              data={dataToRender()}
              subHeaderComponent={
                <CustomHeader
                  store={store}
                  searchTerm={searchTerm}
                  rowsPerPage={rowsPerPage}
                  handleFilter={handleFilter}
                  handlePerPage={handlePerPage}
                  toggleSidebar={toggleSidebar}
                />
              }
            />
          </div>
        ) : (
          <p className="d-block invalid-feedback">
            You can not create price of free plan!
          </p>
        )}
      </Card>

      {sidebarOpen && (
        <PriceSidebar
          open={sidebarOpen}
          refreshTable={refreshTable}
          toggleSidebar={toggleSidebar}
          planId={planId}
        />
      )}
    </Fragment>
  );
};

export default PricesList;
