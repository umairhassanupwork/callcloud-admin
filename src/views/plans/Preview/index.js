// ** React Imports
import { useState, useEffect, Fragment } from "react";

// ** Third Party Components

import { useSelector, useDispatch } from "react-redux";
import { getActivePlans } from "@store/plans";

// ** Demo Components
// import PricingFaqs from './PricingFaqs'
import PricingCards from "./PricingCards";
// import PricingTrial from './PricingTrial'
import PricingHeader from "./PricingHeader";

// ** Styles
import "@styles/base/pages/page-pricing.scss";

const Pricing = () => {
  // ** States
  const [data, setData] = useState(null),
    [duration, setDuration] = useState("monthly");

  const planStore = useSelector((store) => store.plans);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getActivePlans());
  }, []);

  useEffect(() => {
    const tempData = planStore.activePlans.map((plan) => {
      return {
        id: plan.id,
        title: plan.name,
        subtitle: plan.description,
        // prettier-ignore
        monthlyPrice: plan.active_prices.filter((price) => price.cycle === "monthly").length ? plan.active_prices.filter((price) => price.cycle === "monthly")[0].amount : 0,
        yearlyPlan: {
          // prettier-ignore
          perMonth: plan.active_prices.filter((price) => price.cycle === "yearly").length ? Math.round(plan.active_prices.filter((price) => price.cycle === "yearly")[0].amount / 12 * 10) / 10 : 0,
          // prettier-ignore
          totalAnnual : plan.active_prices.filter((price) => price.cycle === "yearly").length ? plan.active_prices.filter((price) => price.cycle === "yearly")[0].amount : 0,
        },
        // prettier-ignore
        planBenefits: plan.features.map((feature) => feature.title),
      };
    });
    setData(tempData);
  }, [planStore]);

  // basicPlan: {
  //   title: 'Basic',
  //   img: require('@src/assets/images/illustration/Pot1.svg').default,
  //   subtitle: 'A simple start for everyone',
  //   monthlyPrice: 0,
  //   yearlyPlan: {
  //     perMonth: 0,
  //     totalAnnual: 0
  //   },
  //   planBenefits: [
  //     '100 responses a month',
  //     'Unlimited forms and surveys',
  //     'Unlimited fields',
  //     'Basic form creation tools',
  //     'Up to 2 subdomains'
  //   ],
  //   popular: false
  // },

  return (
    <div id="pricing-table">
      <PricingHeader duration={duration} setDuration={setDuration} />
      {data !== null ? (
        <Fragment>
          <PricingCards
            fullWidth={true}
            setData={setData}
            data={data}
            duration={duration}
          />
        </Fragment>
      ) : null}
    </div>
  );
};

export default Pricing;
