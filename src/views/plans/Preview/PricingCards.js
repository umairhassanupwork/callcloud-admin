import { useRef } from "react";
// ** Third Party Components
import classnames from "classnames";

// ** Reactstrap Imports
import {
  Row,
  Col,
  Card,
  CardBody,
  CardText,
  Badge,
  ListGroup,
  ListGroupItem,
  Button,
} from "reactstrap";

import { ReactSortable } from "react-sortablejs";
import { orderProducts } from "@store/plans";
import { useDispatch } from "react-redux";

const PricingCards = ({
  data,
  duration,
  bordered,
  fullWidth,
  cols,
  setData,
}) => {
  const colsProps = cols ? cols : { md: data.length > 3 ? 3 : 4, xs: 12 };

  const renderPricingCards = () => {
    const stateRef = useRef();
    const dispatch = useDispatch();

    return (
      <ReactSortable
        className="d-flex flex-row justify-content-around w-100"
        draggable=".item"
        list={data}
        onEnd={() => {
          const orderedIds = stateRef.current.map((product) => product.id);
          dispatch(orderProducts({ orderedIds })).then(
            () => (stateRef.current = null)
          );
        }}
        setList={(items) => {
          setData(items);
          //   setFeatures(items);
          stateRef.current = items;
        }}
      >
        {data.map((item, index) => {
          // prettier-ignore
          const monthlyPrice = duration === "yearly" ? item.yearlyPlan.perMonth : item.monthlyPrice,
            // prettier-ignore
            yearlyPrice = duration === "yearly" ? item.yearlyPlan.totalAnnual : item.monthlyPrice;
          // imgClasses = item.title === 'Basic' ? 'mb-2 mt-5' : item.title === 'Standard' ? 'mb-1' : 'mb-2'

          return (
            <Col className="item px-1" key={index} {...colsProps}>
              <Card
                style={{ cursor: "all-scroll" }}
                className={classnames("text-center", {
                  border: bordered,
                  "shadow-none": bordered,
                  popular: item.popular === true,
                  "border-primary": bordered && item.popular === true,
                  [`${item.title.toLowerCase()}-pricing`]: item.title,
                })}
              >
                <CardBody>
                  {item.popular === true ? (
                    <div className="pricing-badge text-end">
                      <Badge color="light-primary" pill>
                        Popular
                      </Badge>
                    </div>
                  ) : null}
                  {/* <img className={imgClasses} src={item.img} alt='pricing svg' /> */}
                  <h3>{item.title}</h3>
                  <CardText>{item.subtitle}</CardText>
                  <div className="annual-plan">
                    <div className="plan-price mt-2">
                      <sup className="font-medium-1 fw-bold text-primary me-25">
                        $
                      </sup>
                      <span
                        className={`pricing-${item.title.toLowerCase()}-value fw-bolder text-primary`}
                      >
                        {monthlyPrice}
                      </span>
                      <span className="pricing-duration text-body font-medium-1 fw-bold ms-25">
                        /month
                      </span>
                    </div>
                    {item.title !== "Basic_" && duration === "yearly" ? (
                      <small className="annual-pricing text-muted">
                        USD {yearlyPrice} / year
                      </small>
                    ) : null}
                  </div>
                  <ListGroup
                    tag="ul"
                    className="list-group-circle text-start mb-2"
                  >
                    {item.planBenefits.map((benefit, i) => (
                      <ListGroupItem key={i} tag="li">
                        {benefit}
                      </ListGroupItem>
                    ))}
                  </ListGroup>
                  <Button
                    disabled
                    block
                    outline={item.title !== "Standard"}
                    color={item.title === "Basic" ? "success" : "primary"}
                  >
                    {item.title === "Basic" ? "Your current plan" : "Upgrade"}
                  </Button>
                </CardBody>
              </Card>
            </Col>
          );
        })}
      </ReactSortable>
    );
  };

  const defaultCols = {
    sm: { offset: 2, size: 10 },
    lg: { offset: 2, size: 10 },
  };

  return (
    <Row className="pricing-card">
      <Col
        {...(!fullWidth ? defaultCols : {})}
        className={classnames({ "mx-auto": !fullWidth })}
      >
        <Row>{renderPricingCards()}</Row>
      </Col>
    </Row>
  );
};

export default PricingCards;
