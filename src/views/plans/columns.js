// ** React Imports
import { Link } from "react-router-dom";

// ** Custom Components
import Avatar from "@components/avatar";

// ** Store & Actions
// import { store } from "@store/store";
// import { deleteWorkspace } from "@store/workspaces";

// ** Icons Imports
import {
  Slack,
  User,
  Settings,
  Database,
  Edit2,
  MoreVertical,
  FileText,
  Trash2,
  Archive,
} from "react-feather";

// ** Reactstrap Imports
import {
  Badge,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
  Label,
} from "reactstrap";

// const renderName = (row) => {
//   return row.nickname ? row.nickname : row.name;
// };
const renderCreatedAt = (row) => {
  return row.createdAt ? row.createdAt : "-";
};

// const renderJoinedAt = (row) => {
//   return row.joinedAt ? row.joinedAt : "-";
// };

// const renderClient = (row) => {
//   if (row.avatar && row.avatar.length) {
//     return <Avatar className="me-1" img={row.avatar} width="32" height="32" />;
//   } else {
//     return (
//       <Avatar
//         initials
//         className="me-1"
//         color={row.avatarColor || "light-primary"}
//         content={row.name || "John Doe"}
//       />
//     );
//   }
// };

// const renderWorkspaceLogo = (row) => {
//   if (row.logo && row.logo.length) {
//     return <Avatar className="me-1" img={row.logo} width="32" height="32" />;
//   } else {
//     return (
//       <Avatar
//         initials
//         className="me-1"
//         color={row.avatarColor || "light-primary"}
//         content={row.name || "John Doe"}
//       />
//     );
//   }
// };
const Title = () => (
  <div
    style={{ paddingLeft: "45px" }}
    className="d-flex justify-content-center align-items-center"
  >
    Name
  </div>
);
export const planTableColumns = [
  {
    name: <Title />,
    sortable: true,
    minWidth: "200px",
    sortField: "name",
    selector: (row) => row.name,
    cell: (row) => row.name,
  },
  {
    name: "Created At",
    sortable: true,
    minWidth: "172px",
    sortField: "createdAt",
    selector: (row) => row.createdAt,
    cell: (row) => renderCreatedAt(row),
  },

  {
    name: "Status",
    sortable: false,
    minWidth: "172px",
    cell: (row) => (
      <div className="d-flex flex-column">
        <div className="form-switch form-check-primary">
          <Input
            role="button"
            type="switch"
            id="switch-primary"
            name="primary"
            checked={row.active}
            onChange={(e) => {
              console.log(e.target.checked);
              row.handleStatusChange(row.id, e.target.checked);
            }}
          />
        </div>
      </div>
    ),
  },

  {
    name: "Actions",
    minWidth: "100px",
    cell: (row) => (
      <div className="column-action">
        <UncontrolledDropdown>
          <DropdownToggle tag="div" className="btn btn-sm">
            <MoreVertical size={14} className="cursor-pointer" />
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem
              tag={Link}
              className="w-100"
              to={`/plan/${row.id}`}
              // onClick={() => store.dispatch(getUser(row.id))}
            >
              <FileText size={14} className="me-50" />
              <span className="align-middle">Manage Prices and Features</span>
            </DropdownItem>
            <DropdownItem
              tag="a"
              href="/"
              className="w-100"
              onClick={(e) => {
                row.handleEdit(row.id);
                e.preventDefault();
              }}
            >
              <Archive size={14} className="me-50" />
              <span className="align-middle">Edit</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </div>
    ),
  },
];

export const priceTableColumns = [
  {
    name: "Billing Cycle",
    sortable: false,
    minWidth: "200px",
    cell: (row) => {
      return row.cycle === "monthly" ? "Monthly" : "Yearly";
    },
  },
  {
    name: "Amount",
    sortable: true,
    minWidth: "172px",
    sortField: "amount",
    selector: (row) => row.amount,
    cell: (row) => {
      return `$${row.amount}`;
    },
  },

  {
    name: "Created At",
    sortable: true,
    minWidth: "172px",
    sortField: "createdAt",
    selector: (row) => row.createdAt,
    cell: (row) => renderCreatedAt(row),
  },

  {
    name: "Status",
    sortable: false,
    minWidth: "172px",
    cell: (row) => (
      <div className="d-flex flex-column">
        <div className="form-switch form-check-primary">
          <Input
            role="button"
            type="switch"
            id="switch-primary"
            name="primary"
            checked={row.active}
            onChange={(e) => {
              console.log(e.target.checked);
              row.handleStatusChange(row.id, e.target.checked);
            }}
          />
        </div>
      </div>
    ),
  },

  // {
  //   name: "Actions",
  //   minWidth: "100px",
  //   cell: (row) => (
  //     <div className="column-action">
  //       <UncontrolledDropdown>
  //         <DropdownToggle tag="div" className="btn btn-sm">
  //           <MoreVertical size={14} className="cursor-pointer" />
  //         </DropdownToggle>
  //         <DropdownMenu>
  //           <DropdownItem
  //             tag={Link}
  //             className="w-100"
  //             to={`/plan/${row.id}`}
  //             // onClick={() => store.dispatch(getUser(row.id))}
  //           >
  //             <FileText size={14} className="me-50" />
  //             <span className="align-middle">Manage Prices and Features</span>
  //           </DropdownItem>
  //           <DropdownItem
  //             tag="a"
  //             href="/"
  //             className="w-100"
  //             onClick={(e) => {
  //               row.handleEdit(row.id);
  //               e.preventDefault();
  //             }}
  //           >
  //             <Archive size={14} className="me-50" />
  //             <span className="align-middle">Edit</span>
  //           </DropdownItem>
  //         </DropdownMenu>
  //       </UncontrolledDropdown>
  //     </div>
  //   ),
  // },
];
