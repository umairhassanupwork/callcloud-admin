import { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import {
  getFeatures,
  createPlanFeature,
  deletePlanFeature,
  orderPlanFeatures,
} from "@store/plans";

import { Button, Col, Label, Input, Spinner } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThumbTack } from "@fortawesome/free-solid-svg-icons";
import { ReactSortable } from "react-sortablejs";
import toast from "react-hot-toast";

const FeatureList = ({ planId }) => {
  const [features, setFeatures] = useState([]);
  const dispatch = useDispatch();

  const stateRef = useRef();

  useEffect(() => {
    console.log("createPlanFeature", createPlanFeature);
    dispatch(getFeatures({ planId }))
      .unwrap()
      .then((d) => {
        if (d.data.features.length > 0) {
          const formattedFeatures = d.data.features.map((feature) => {
            return {
              id: feature.id,
              title: feature.title,
              order_number: feature.order_number,
              editMode: false,
              error: false,
              saveLoader: false,
              removeLoader: false,
            };
          });
          formattedFeatures.sort((a, b) => a.order_number - b.order_number);
          setFeatures(formattedFeatures);
        } else {
          setFeatures([
            {
              id: null,
              title: "",
              order_number: 1,
              editMode: true,
              error: false,
              saveLoader: false,
              removeLoader: false,
            },
          ]);
        }
      });
  }, []);
  return (
    <>
      <ReactSortable
        draggable=".item"
        list={features}
        onEnd={() => {
          const orderedIds = stateRef.current
            .filter((feature) => feature.id)
            .map((feature) => feature.id);
          dispatch(orderPlanFeatures({ id: planId, orderedIds })).then(
            () => (stateRef.current = null)
          );
        }}
        setList={(items) => {
          setFeatures(items);
          stateRef.current = items;
        }}
      >
        {features.map((feature, index) => {
          return (
            <Col
              sm="7"
              lg="10"
              key={index}
              className={
                // prettier-ignore
                !feature.editMode ? "my-2 p-1 rounded bg-light item" : "my-2 p-1 rounded bg-light"
              }
            >
              <Label className="form-label" for={`name-${index}`}>
                {`Feature ${index + 1}`}
              </Label>
              <div className="d-flex align-items-start justify-content-around">
                {!feature.editMode && (
                  <FontAwesomeIcon
                    fontSize={20}
                    icon={faThumbTack}
                    data-tour="toggle-icon"
                    className="toggle-icon d-none d-xl-block"
                    style={{ cursor: "all-scroll" }}
                    //   onClick={() => setMenuCollapsed(true)}
                  />
                )}

                <div className="flex-grow-1 d-flex align-items-center">
                  {feature.editMode ? (
                    <Input
                      id={`title-${index}`}
                      placeholder="Feature of the plan"
                      // invalid={newWorkspace.error}
                      value={feature.title}
                      onChange={(e) => {
                        // setName(name);
                        const tempFeatures = features.map((feature, i) => {
                          if (i === index) {
                            feature.title = e.target.value;
                          }
                          return feature;
                        });
                        tempFeatures.sort(
                          (a, b) => a.order_number - b.order_number
                        );
                        setFeatures(tempFeatures);
                      }}
                    />
                  ) : (
                    <span>{feature.title}</span>
                  )}
                </div>
                <div className="ms-2">
                  {index === features.length - 1 ? (
                    <>
                      {!feature.editMode && (
                        <Button
                          onClick={() => {
                            if (feature.id) {
                              const tempFeatures = features.map(
                                (feature, i) => {
                                  if (i === index) {
                                    feature.removeLoader = true;
                                  }
                                  return feature;
                                }
                              );
                              setFeatures(tempFeatures);
                              dispatch(
                                deletePlanFeature({ id: feature.id })
                              ).then(() => {
                                const tempFeatures = features.filter(
                                  (feature, i) => {
                                    return i !== index;
                                  }
                                );
                                if (tempFeatures.length) {
                                  setFeatures(tempFeatures);
                                } else {
                                  setFeatures([
                                    {
                                      id: null,
                                      title: "",
                                      order_number: 1,
                                      editMode: true,
                                      error: false,
                                      saveLoader: false,
                                      removeLoader: false,
                                    },
                                  ]);
                                }
                              });
                            }
                          }}
                          className="me-1"
                          color="secondary"
                          size="sm"
                          outline
                        >
                          &nbsp;Remove&nbsp;
                          {feature.removeLoader && (
                            <Spinner
                              style={{ marginLeft: "5px" }}
                              size={"sm"}
                              color="white"
                            />
                          )}
                        </Button>
                      )}

                      {feature.editMode && (
                        <Button
                          onClick={() => {
                            if (!feature.title) {
                              toast.error("Please add title!");
                              return;
                            }
                            const tempFeatures = features.map((feature, i) => {
                              if (i === index) {
                                feature.saveLoader = true;
                              }
                              return feature;
                            });

                            setFeatures(tempFeatures);

                            dispatch(
                              createPlanFeature({
                                planId,
                                title: feature.title,
                              })
                            )
                              .unwrap()
                              .then((d) => {
                                const tempFeatures = features.map(
                                  (feature, i) => {
                                    if (i === index) {
                                      feature.editMode = false;
                                      feature.order_number =
                                        d.data.feature.order_number;
                                      feature.id = d.data.feature.id;
                                      feature.saveLoader = false;
                                    }
                                    return feature;
                                  }
                                );
                                tempFeatures.sort(
                                  (a, b) => a.order_number - b.order_number
                                );
                                setFeatures(tempFeatures);
                              });
                          }}
                          className="me-1"
                          color="primary"
                          size="sm"
                        >
                          Save
                          {feature.saveLoader && (
                            <Spinner
                              style={{ marginLeft: "5px" }}
                              size={"sm"}
                              color="white"
                            />
                          )}
                        </Button>
                      )}
                      <Button
                        onClick={() => {
                          const tempFeatures = [...features];
                          tempFeatures.push({
                            title: "",
                            error: false,
                            editMode: true,
                          });
                          setFeatures(tempFeatures);
                        }}
                        className="me-1"
                        color="primary"
                        size="sm"
                      >
                        Add More
                      </Button>
                    </>
                  ) : (
                    <>
                      <Button
                        onClick={() => {
                          if (feature.id) {
                            const tempFeatures = features.map((feature, i) => {
                              if (i === index) {
                                feature.removeLoader = true;
                              }
                              return feature;
                            });
                            setFeatures(tempFeatures);
                            dispatch(
                              deletePlanFeature({ id: feature.id })
                            ).then(() => {
                              const tempFeatures = features.filter(
                                (feature, i) => {
                                  return i !== index;
                                }
                              );

                              if (tempFeatures.length) {
                                setFeatures(tempFeatures);
                              } else {
                                setFeatures([
                                  {
                                    id: null,
                                    title: "",
                                    order_number: 1,
                                    editMode: true,
                                    error: false,
                                    saveLoader: false,
                                    removeLoader: false,
                                  },
                                ]);
                              }
                            });
                          } else {
                            const tempFeatures = features.filter(
                              (feature, i) => {
                                return i !== index;
                              }
                            );

                            if (tempFeatures.length) {
                              setFeatures(tempFeatures);
                            } else {
                              setFeatures([
                                {
                                  id: null,
                                  title: "",
                                  order_number: 1,
                                  editMode: true,
                                  error: false,
                                  saveLoader: false,
                                  removeLoader: false,
                                },
                              ]);
                            }
                          }
                        }}
                        className="me-1"
                        color="secondary"
                        size="sm"
                        outline
                      >
                        &nbsp;Remove&nbsp;
                        {feature.removeLoader && (
                          <Spinner
                            style={{ marginLeft: "5px" }}
                            size={"sm"}
                            color="white"
                          />
                        )}
                      </Button>
                      {feature.editMode && (
                        <Button
                          onClick={() => {
                            if (!feature.title) {
                              toast.error("Please add title!");
                              return;
                            }
                            const tempFeatures = features.map((feature, i) => {
                              if (i === index) {
                                feature.saveLoader = true;
                              }
                              return feature;
                            });

                            setFeatures(tempFeatures);

                            dispatch(
                              createPlanFeature({
                                planId,
                                title: feature.title,
                              })
                            )
                              .unwrap()
                              .then((d) => {
                                const tempFeatures = features.map(
                                  (feature, i) => {
                                    if (i === index) {
                                      feature.editMode = false;
                                      feature.order_number =
                                        d.data.feature.order_number;
                                      feature.id = d.data.feature.id;
                                      feature.saveLoader = false;
                                    }
                                    return feature;
                                  }
                                );
                                tempFeatures.sort(
                                  (a, b) => a.order_number - b.order_number
                                );
                                setFeatures(tempFeatures);
                              });
                          }}
                          className="me-1"
                          color="primary"
                          size="sm"
                        >
                          Save
                          {feature.saveLoader && (
                            <Spinner
                              style={{ marginLeft: "5px" }}
                              size={"sm"}
                              color="white"
                            />
                          )}
                        </Button>
                      )}
                    </>
                  )}
                </div>
              </div>
            </Col>
          );
        })}
      </ReactSortable>
    </>
  );
};

export default FeatureList;
