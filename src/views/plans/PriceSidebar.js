// ** React Import
import { useState, useEffect } from "react";

// ** Custom Components
import Sidebar from "@components/sidebar";
import Select from "react-select";
// ** Utils
import { selectThemeColors } from "@utils";

// ** Third Party Components

// ** Reactstrap Imports
import {
  Button,
  Label,
  Form,
  Input,
  FormFeedback,
  Spinner,
  InputGroupText,
  InputGroup,
} from "reactstrap";

// ** Store & Actions
import { createPrice } from "@store/plans";
import { useDispatch, useSelector } from "react-redux";

const PriceSidebar = ({ open, toggleSidebar, refreshTable, planId }) => {
  // ** States

  const store = useSelector((state) => state.plans);
  let hasMonthlyPlan = store.prices.filter(
    (price) => price.cycle === "monthly" && price.active
  );

  if (hasMonthlyPlan.length === 0) {
    hasMonthlyPlan = store.prices.filter((price) => price.cycle === "monthly");
  }

  const [cycle, setCycle] = useState({
    id: 1,
    value: "monthly",
    label: "Monthly",
  });

  const [amount, setAmount] = useState("");

  const [amountError, setAmountError] = useState(false);

  const [formSubmissionLoader, setFormSubmissionLoader] = useState(false);
  const [percentOfMonthlyPrice, setPercentOfMonthlyPrice] = useState(100);

  useEffect(() => {
    if (cycle.value === "monthly") {
      setAmount("");
    } else {
      // prettier-ignore
      hasMonthlyPlan.length > 0 ? setAmount(hasMonthlyPlan[0].amount * 12) : setAmount("");
      setPercentOfMonthlyPrice(100);
    }
  }, [cycle]);

  // ** Store Vars
  const dispatch = useDispatch();

  // ** Function to handle form submit
  const onSubmit = (e) => {
    e.preventDefault();

    let valid = true;

    if (!amount) {
      valid = false;
      setAmountError(true);
    } else {
      setAmountError(false);
    }

    if (valid) {
      setFormSubmissionLoader(true);

      if (planId) {
        dispatch(createPrice({ amount, cycle: cycle.value, planId })).then(
          (result) => {
            setFormSubmissionLoader(false);
            if (result.payload.data.price) {
              setAmount(null);
              setCycle({
                id: 1,
                value: "monthly",
                label: "Monthly",
              });
              refreshTable();
              toggleSidebar();
            }
          }
        );
      }
    }
  };

  const handleSidebarClosed = () => {
    setAmount("");
    setCycle({
      id: 1,
      value: "monthly",
      label: "Monthly",
    });
    setAmountError(false);
  };

  return (
    <Sidebar
      size="lg"
      open={open}
      title={"New Price"}
      headerClassName="mb-1"
      contentClassName="pt-0"
      toggleSidebar={toggleSidebar}
      onClosed={handleSidebarClosed}
    >
      <Form>
        <div className="mb-1">
          <Label className="form-label" for="cycle">
            Billing Cycle <span className="text-danger">*</span>
          </Label>

          <Select
            theme={selectThemeColors}
            className="react-select"
            id="cycle"
            classNamePrefix="select"
            options={[
              {
                id: 1,
                value: "monthly",
                label: "Monthly",
              },
              {
                id: 2,
                value: "yearly",
                label: "Yearly",
              },
            ]}
            value={cycle}
            isClearable={false}
            onChange={(selected) => {
              console.log(selected);

              setCycle(selected);
            }}
          />
        </div>
        {hasMonthlyPlan.length > 0 && cycle.value === "yearly" && (
          <div className="mb-1">
            <Label className="form-label" for="percentage">
              % Of Monthly Price ${hasMonthlyPlan[0].amount}{" "}
            </Label>

            <InputGroup className="input-group-merge">
              <Input
                id="percentage"
                placeholder="100"
                // invalid={amountError}
                value={percentOfMonthlyPrice}
                type="number"
                // disabled={pl}
                onChange={(e) => {
                  setPercentOfMonthlyPrice(e.target.value);
                  const amount =
                    (hasMonthlyPlan[0].amount * 12 * e.target.value) / 100;
                  setAmount(amount);
                }}
              />
              <InputGroupText>%</InputGroupText>
            </InputGroup>

            <FormFeedback>Please enter a valid Percentage</FormFeedback>
          </div>
        )}
        <div className="mb-1">
          <Label className="form-label" for="amount">
            Amount <span className="text-danger">*</span>
          </Label>

          <InputGroup className="input-group-merge">
            <InputGroupText>$</InputGroupText>
            <Input
              id="amount"
              placeholder="100"
              // invalid={amountError}
              value={amount}
              type="number"
              // disabled={pl}
              onChange={(e) => {
                console.log("amount changed");
                setAmount(e.target.value);

                if (hasMonthlyPlan.length > 0) {
                  const percentage =
                    (100 * e.target.value) / (hasMonthlyPlan[0].amount * 12);

                  setPercentOfMonthlyPrice(percentage);
                }
              }}
            />
          </InputGroup>

          {amountError && (
            <FormFeedback className="d-block">
              Please enter a valid Amount
            </FormFeedback>
          )}
        </div>

        <Button onClick={(e) => onSubmit(e)} className="me-1" color="primary">
          Submit
          {formSubmissionLoader && (
            <Spinner style={{ marginLeft: "5px" }} size={"sm"} color="white" />
          )}
        </Button>
        <Button type="reset" color="secondary" outline onClick={toggleSidebar}>
          Cancel
        </Button>
      </Form>
    </Sidebar>
  );
};

export default PriceSidebar;
