// ** React Imports
import { Link } from "react-router-dom";

// ** Custom Components
import Avatar from "@components/avatar";

// ** Icons Imports
import { Slack, User, Settings, Database, Edit2, MoreVertical, FileText, Trash2, Archive } from "react-feather";

// ** Reactstrap Imports
import { Badge, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Input, Label } from "reactstrap";

const renderCreatedAt = (row) => {
  return row.createdAt ? row.createdAt : "-";
};

const Name = () => (
  <div style={{ paddingLeft: "25px" }} className="d-flex justify-content-center align-items-center">
    Name
  </div>
);

const Message = () => (
  <div style={{ paddingLeft: "20px" }} className="d-flex justify-content-center align-items-center">
    Message
  </div>
);

export const feedbackTableColumns = [
  {
    name: <Name />,
    sortable: false,
    minWidth: "172px",
    selector: (row) => row.user_name,
    cell: (row) => row.user_name,
  },
  {
    name: "Rating",
    minWidth: "100px",
    sortable: true,
    sortField: "rating",
    selector: (row) => row.rating,
    cell: (row) => row.rating,
  },
  {
    name: <Message />,
    sortable: false,
    minWidth: "500px",
    selector: (row) => row.message,
    cell: (row) =>  row.message,
  },
  {
    name: "Created At",
    sortable: true,
    minWidth: "172px",
    sortField: "createdAt",
    selector: (row) => row.createdAt,
    cell: (row) => renderCreatedAt(row),
  },
];