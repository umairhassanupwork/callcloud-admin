// ** React Imports
import React, { Fragment, useEffect, useState } from "react";

// ** Invoice List Sidebar
import Sidebar from "./Sidebar";

// ** Table Columns
import { feedbackTableColumns } from "./columns";

// ** Store & Actions
import { getFeedbacks, storeCurrentPage, storeRowsPerPage } from "@store/feedbacks";
import { useDispatch, useSelector } from "react-redux";

// ** Third Party Components
import ReactPaginate from "react-paginate";
import DataTable from "react-data-table-component";
import { ChevronDown } from "react-feather";

// ** Reactstrap Imports
import { Row, Col, Card, Input, Button } from "reactstrap";

// ** Styles
import "@styles/react/libs/react-select/_react-select.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";

// ** Table Header
const CustomHeader = ({ handlePerPage, rowsPerPage, handleFilter, searchTerm }) => {
   return (
      <div className="invoice-list-table-header w-100 me-1 ms-50 mt-2 mb-75">
         <Row>
            <Col xl="6" className="d-flex align-items-center p-0">
               <div className="d-flex align-items-center mb-sm-0 mb-1 me-1">
                  <label className="mb-0" htmlFor="search-invoice"></label>
                  <Input id="search-invoice" className="ms-50 w-100" type="text" value={searchTerm} placeholder="Type to find" onChange={(e) => handleFilter(e.target.value)}/>
               </div>
            </Col>

            <Col xl="6" className="d-flex align-items-sm-center justify-content-xl-end justify-content-start flex-xl-nowrap flex-wrap flex-sm-row flex-column pe-xl-1 p-0 mt-xl-0 mt-1">
               <div className="d-flex align-items-center mx-1">
                  <label htmlFor="rows-per-page">Show</label>
                  <Input className="mx-50" type="select" id="rows-per-page" value={rowsPerPage} onChange={handlePerPage} style={{ width: "5rem" }}>
                     <option value="10">10</option>
                     <option value="25">25</option>
                     <option value="50">50</option>
                  </Input>
               </div>
            </Col>
         </Row>
      </div>
   );
};

const FeedbacksList = () => {
   // ** Store Vars
   const dispatch = useDispatch();
   const store    = useSelector((state) => state.feedbacks);

   // ** States
   const [sort, setSort]               = useState("desc");
   const [searchTerm, setSearchTerm]   = useState("");
   const [currentPage, setCurrentPage] = useState(() => store.currentPage);
   const [sortColumn, setSortColumn]   = useState("");
   const [rowsPerPage, setRowsPerPage] = useState(() => store.rowsPerPage);

   // const refreshTable = () => {
   //    dispatch(
   //       getFeedbacks({
   //          sort,
   //          sortColumn,
   //          q: searchTerm,
   //          page: currentPage,
   //          perPage: rowsPerPage,
   //       })
   //    );
   // };

   // ** Get data on mount
   useEffect(() => {
      dispatch(
         getFeedbacks({
            sort,
            sortColumn,
            q: searchTerm,
            page: currentPage,
            perPage: rowsPerPage,
         })
      );
   }, []);

   // ** Function in get data on page change
   const handlePagination = (page) => {
      dispatch(
         getFeedbacks({
            sort,
            sortColumn,
            q: searchTerm,
            perPage: rowsPerPage,
            page: page.selected + 1,
         })
      );
      const newPage = page.selected + 1;
      setCurrentPage(newPage);
      dispatch(storeCurrentPage({ currentPage: newPage }));
   };

   // ** Function in get data on rows per page
   const handlePerPage = (e) => {
      const value = parseInt(e.currentTarget.value);
      dispatch(
         getFeedbacks({
            sort,
            sortColumn,
            q: searchTerm,
            perPage: value,
            page: 1,
         })
      );
      setCurrentPage(1);
      setRowsPerPage(value);
      dispatch(storeCurrentPage({ currentPage: 1 }));
      dispatch(storeRowsPerPage({ rowsPerPage: value }));
   };

   // ** Function in get data on search query change
   const handleFilter = (val) => {
      let page = currentPage;
      if (val && !searchTerm) {
         page = 1;
         setCurrentPage(1);
      }

      setSearchTerm(val);
      dispatch(
         getFeedbacks({
            sort,
            q: val,
            sortColumn,
            page,
            perPage: rowsPerPage,
         })
      );
   };

   // ** Custom Pagination
   const CustomPagination = () => {
      const count = Number(Math.ceil(store.totalFeedbacks / rowsPerPage));

      return (
         <ReactPaginate previousLabel={""} nextLabel={""} pageCount={count || 1} activeClassName="active" forcePage={currentPage !== 0 ? currentPage - 1 : 0} onPageChange={(page) => handlePagination(page)} pageClassName={"page-item"} nextLinkClassName={"page-link"} nextClassName={"page-item next"} previousClassName={"page-item prev"} previousLinkClassName={"page-link"} pageLinkClassName={"page-link"}
         containerClassName={ "pagination react-paginate justify-content-end my-2 pe-1" } />
      );
   };

   // ** Table data to render
   const dataToRender = () => {
      const filters = {
         q: searchTerm,
      };

      const isFiltered = Object.keys(filters).some(function (k) {
         return filters[k].length > 0;
      });

      if (store.feedbacks.length > 0) {
         const feedbacks = store.feedbacks.map((feedback) => {
            const tempFeedback = { ...feedback };
            //    tempTimezone.handleEdit = (id) => {
            //       const editTimezone = store.timezones.filter((timezone) => timezone.id === id);
            //       if (editTimezone.length) {
            //          setEditTimezone(editTimezone[0]);
            //          toggleSidebar();
            //       }
            //    };
               return tempFeedback;
            });
         return feedbacks;
      } else if (store.feedbacks.length === 0 && isFiltered) {
         return [];
      } else {
         return store.feedbacks.slice(0, rowsPerPage);
      }
   };

   const handleSort = (column, sortDirection) => {
      console.log(column, sortDirection);
      setSort(sortDirection);
      setSortColumn(column.sortField);
      dispatch(
         getFeedbacks({
            sort: sortDirection,
            sortColumn: column.sortField,
            q: searchTerm,
            page: currentPage,
            perPage: rowsPerPage,
         })
      );
   };

   return (
      <Fragment>
         <Card className="overflow-hidden workspace-list">
            <div className="react-dataTable">
               <DataTable noHeader subHeader sortServer pagination responsive paginationServer columns={feedbackTableColumns} onSort={handleSort} sortIcon={<ChevronDown />} className="react-dataTable" paginationComponent={CustomPagination} data={dataToRender()}
                  subHeaderComponent={ 
                     <CustomHeader store={store} searchTerm={searchTerm} rowsPerPage={rowsPerPage} handleFilter={handleFilter} handlePerPage={handlePerPage}/>
                  }
               />
            </div>
         </Card>
      </Fragment>
   );
};

export default FeedbacksList;
