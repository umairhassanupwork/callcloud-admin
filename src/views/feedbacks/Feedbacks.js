import Table from "./Table";

const Feedbacks = () => {
  return (
    <div className="app-user-list">
      <Table />
    </div>
  );
};

export default Feedbacks;
