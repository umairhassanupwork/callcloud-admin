// ** React Import
import { useState } from "react";

// ** Custom Components
import Sidebar from "@components/sidebar";

// ** Third Party Components

// ** Reactstrap Imports
import { Button, Label, Form, Input, FormFeedback, Spinner } from "reactstrap";

// ** Store & Actions
import { createTimezone, updateTimezone } from "@store/timezones";
import { useDispatch } from "react-redux";

const TimezoneSidebar = ({ open, toggleSidebar, refreshTable, timezonee = null }) => {
  /* state vars */
  const [name, setName] = useState(() => {
    return timezonee ? timezonee.name : "";
  });

  const [identifier, setIdentifier] = useState(() => {
    return timezonee ? timezonee.identifier : "";
  });

  const [group, setGroup] = useState(() => {
    return timezonee ? timezonee.group : "";
  });

  const [nameError, setNameError]                       = useState(false);
  const [identifierError, setIdentifierError]           = useState(false);
  const [groupError, setGroupError]                     = useState(false);
  const [formSubmissionLoader, setFormSubmissionLoader] = useState(false);

  // ** Store Vars
  const dispatch = useDispatch();

  // ** Function to handle form submit
  const onSubmit = (e) => {
    e.preventDefault();

    let valid = true;

    if (!name) {
      valid = false;
      setNameError(true);
    } else {
      setNameError(false);
    }

    if (!identifier) {
      valid = false;
      setIdentifierError(true);
    } else {
      setIdentifierError(false);
    }

    if (!group) {
      valid = false;
      setGroupError(true);
    } else {
      setGroupError(false);
    }

    if (valid) {
      setFormSubmissionLoader(true);

      if (timezonee) {
        dispatch(updateTimezone({ name, identifier, group, id: timezonee.id })).then(
          (result) => {
            setFormSubmissionLoader(false);
            if (result.payload.data.timezone) {
              setName("");
              setIdentifier("");
              setGroup("");
              refreshTable();
              toggleSidebar();
            }
          }
        );
      } else {
        dispatch(createTimezone({ name, identifier, group })).then((result) => {
          setFormSubmissionLoader(false);
          if (result.payload.data.timezone) {
            setName("");
            setIdentifier("");
            setGroup("");
            refreshTable();
            toggleSidebar();
          }
        });
      }
    }
  };

  const handleSidebarClosed = () => {
    setName("");
    setIdentifier("");
    setGroup("");

    setNameError(false);
    setIdentifierError(false);
    setGroupError(false);
  };

  return (
    <Sidebar size="lg" open={open} title={timezonee ? "Edit Timezone" : "New Timezone"} headerClassName="mb-1" contentClassName="pt-0"
      toggleSidebar={toggleSidebar} onClosed={handleSidebarClosed}>
      <Form>
        <div className="mb-1">
          <Label className="form-label" for="name">
            Name <span className="text-danger">*</span>
          </Label>

          <Input id="name" placeholder="Eastern Time - US & Canada" invalid={nameError} value={name} onChange={(e) => setName(e.target.value)}/>
          <FormFeedback>Please enter a valid name</FormFeedback>
        </div>

        <div className="mb-1">
          <Label className="form-label" for="identifier">
            Identifier <span className="text-danger">*</span>
          </Label>

          <Input id="identifier" placeholder="America/New_York" invalid={identifierError} value={identifier} onChange={(e) => setIdentifier(e.target.value)}/>
          <FormFeedback>Please enter a valid identifier</FormFeedback>
        </div>

        <div className="mb-1">
          <Label className="form-label" for="group">
            Group <span className="text-danger">*</span>
          </Label>

          <Input id="group" placeholder="America/New_York" invalid={groupError} value={group} onChange={(e) => setGroup(e.target.value)}/>
          <FormFeedback>Please enter a valid group</FormFeedback>
        </div>

        <Button onClick={(e) => onSubmit(e)} className="me-1" color="primary">
          Submit {formSubmissionLoader && (
            <Spinner style={{ marginLeft: "5px" }} size={"sm"} color="white" />
          )}
        </Button>
        <Button type="reset" color="secondary" outline onClick={toggleSidebar}>
          Cancel
        </Button>
      </Form>
    </Sidebar>
  );
};

export default TimezoneSidebar;
