import { useParams } from "react-router-dom";
import { lazy } from "react";
// const Workspaces = lazy(() => import("../../views/workspaces/Workspaces"));
const Table = lazy(() => import("../../views/workspaces/Table"));

const CompanyDetails = () => {
  const params = useParams();
  //   return <p>{`CompanyDetails ${params.id}`}</p>;
  return <Table company={params.id} />;
};

export default CompanyDetails;
