import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardText,
  CardLink,
} from "reactstrap";

import { Navigate } from "react-router-dom";

import { useSelector } from "react-redux";

const Home = () => {
  const store = useSelector((state) => {
    return state.auth;
  });

  if (!store.user) {
    return <Navigate to="/login" />;
  }

  // if (workspaceState.currentWorkspace) {
  //   return <Navigate to={`/workspace/${workspaceState.currentWorkspace.id}`} />;
  // }

  return (
    <div>
      <Card>
        <CardHeader>
          <CardTitle>
            {
              // prettier-ignore
              `Welcome ${store.user.name} (${store.user.email}) to admin dashboard 🚀`
            }
          </CardTitle>
        </CardHeader>
        <CardBody>
          <CardText>Manage users and workspaces here.</CardText>
        </CardBody>
      </Card>
    </div>
  );
};

export default Home;
