// ** React Imports
import { Fragment, useEffect, useState } from "react";

// ** Axios Imports
import axios from "axios";
axios.defaults.withCredentials = true;

// ** Third Party Components

// ** Reactstrap Imports
import { Row, Col, Form, Card, Input, Label, Button, CardBody, CardTitle, CardHeader, FormFeedback, Spinner } from "reactstrap";

// ** Store & Actions
import { updateSetting } from "@store/settings";
import { useDispatch } from "react-redux";

const Settings = () => {
  // ** Store Vars
  const dispatch = useDispatch();

  // ** State
  const [formSubmissionLoader, setFormSubmissionLoader] = useState(false);
  const [settings, setSettings]                         = useState([]);

  // ** Get data on mount
  useEffect(() => {
    axios.get(`${process.env.REACT_APP_API_ENDPOINT}/api/admin/settings`).then(response => setSettings(response.data.settings))
    // dispatch(getSettings()).then((result) => setSettings(result.payload.data.settings));
  }, []);

  const handleFormChange = (index, event) => {
    const data = [...settings];

    console.log(data);
    
    if (event.target.name === 'value') {
      data[index][event.target.name] = event.target.checked === true ? '1' : '0';
    } else {
      data[index][event.target.name] = event.target.value;
    }

    setSettings(data);
  }

  const submit = (e) => {
    e.preventDefault();
    setFormSubmissionLoader(true);

    dispatch(updateSetting(settings)).then(() => setFormSubmissionLoader(false));
  }

  return (
    <Fragment>
      <Card>
        <CardHeader className="border-bottom">
          <CardTitle tag="h4">Settings</CardTitle>
        </CardHeader>
        <CardBody className="my-25">
          <Form onSubmit={submit}>
          { settings !== null ? (settings.map((setting, index) => {
            return (
              <div key={index}>
                <Row className='justify-content-between align-items-center my-2'>
                  <Col md={3} className='mb-md-0 mb-1'>
                    <Label className='form-label' for={`title-${index}`}>Title</Label>
                    <Input type='text' id={`title-${index}`} name='title' value={setting.title} onChange={event => handleFormChange(index, event)} readOnly/>
                  </Col>
                  { setting.key === 'maintenance_mode' ? <Col md={1} className='mb-md-0 mb-1 pr-0'>
                    <div className='form-check form-switch mt-2'><Input type='switch' name='value' id={`value-${index}`} defaultChecked={!!+setting.value} onChange={event => handleFormChange(index, event)} /></div> </Col> : <Col md={1} className='mb-md-0 mb-1 pr-0'>
                    <Label className='form-label' for={`value-${index}`}>Value</Label>
                    <Input type='text' id={`value-${index}`} name='value' value={setting.value} onChange={event => handleFormChange(index, event)} />
                  </Col> }
                  <Col md={8} className='mb-md-0 mb-1'>
                    <Label className='form-label' for={`message-${index}`}>Message</Label>
                    <Input type='text' id={`message-${index}`} name='message' value={setting.message} onChange={event => handleFormChange(index, event)} />
                  </Col>
                </Row>
              </div>
            )
          })) : null }
            
          <Col className="mt-2 d-flex" sm="12">
            <Button className="me-1" color="primary" onClick={submit}>
              Save changes 
                {formSubmissionLoader && (
                  <Spinner style={{ marginLeft: "5px" }} size={"sm"} color="white" />
                )}
            </Button>
          </Col>
          </Form>
        </CardBody>
      </Card>
    </Fragment>
  );
};

export default Settings;
