import Table from "./Table";

const Notifications = () => {
  return (
    <div className="app-user-list">
      <Table />
    </div>
  );
};

export default Notifications;
