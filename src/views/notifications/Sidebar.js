// ** React Import
import { useState } from "react";

// ** Axios Imports
import axios from "axios";
axios.defaults.withCredentials = true;

// ** Custom Components
import Sidebar from "@components/sidebar";

// ** Third Party Components

// ** Reactstrap Imports
import { Button, Row, Col, Label, Form, Input, FormFeedback, Spinner } from "reactstrap";

// ** Store & Actions
import { createNotification } from "@store/notifications";
import { useDispatch } from "react-redux";

import Select from 'react-select'

// ** Utils
import { selectThemeColors } from '@utils'

const typeOptions = [{ value: '1', label: 'Maintenance' }];

const NotificationSidebar = ({ open, toggleSidebar, refreshTable, notification = null }) => {
  /* state vars */
  const [type, setType] = useState(1);

  // const [members, setMembers] = useState("");

  const [message, setMessage] = useState("");  

  // const [membersOptions, setMembersOptions] = useState("");

  const [typeError, setTypeError]                       = useState(false);
  // const [membersError, setMembersError]                 = useState(false);
  const [messageError, setMessageError]                 = useState(false);
  const [formSubmissionLoader, setFormSubmissionLoader] = useState(false);

  // ** Store Vars
  const dispatch = useDispatch();

  // useEffect(() => {
  //   const membersList = async () => {
  //     await  axios.get(`${process.env.REACT_APP_API_ENDPOINT}/api/admin/members_list`).then(response => setMembersOptions(response.data.members_list))
  //   }
  //   membersList();
  // }, []);

  // const selectAllOptions = () => {
  //   setMembers(membersOptions)

  //   console.log(members)
  // }

  // ** Function to handle form submit
  const onSubmit = (e) => {
    e.preventDefault();

    let valid = true;

    if (!type) {
      valid = false;
      setTypeError(true);
    } else {
      setTypeError(false);
    }

    // if (!members) {
    //   valid = false;
    //   setMembersError(true);
    // } else {
    //   setMembersError(false);
    // }

    if (!message) {
      valid = false;
      setMessageError(true);
    } else {
      setMessageError(false);
    }

    if (valid) {
      setFormSubmissionLoader(true);

      dispatch(createNotification({ type, message })).then((result) => {
        setFormSubmissionLoader(false);
        if (result.payload.data) {
          setType("");
          // setMembers("");
          setMessage("");
          refreshTable();
          toggleSidebar();
        }
      });
    }
  };

  const handleSidebarClosed = () => {
    setType("");
    // setMembers("");
    setMessage("");

    setTypeError(false);
    // setMembersError(false);
    setMessageError(false);
  };

  return (
    <Sidebar size="lg" open={open} title={notification ? "Edit Site Warning" : "New Site Warning"} headerClassName="mb-1" contentClassName="pt-0"
      toggleSidebar={toggleSidebar} onClosed={handleSidebarClosed}>
      <Form>
        <div className="mb-1">
          <Label className="form-label" for="type">
            Type <span className="text-danger">*</span>
          </Label>

          <Select theme={selectThemeColors} id="type" className='react-select' classNamePrefix='select' defaultValue={typeOptions[0]} options={typeOptions} 
            isClearable={false}/>
          {typeError && (<div className="invalid-feedback" style={{ display: "block" }}>Please select Type</div>) }
        </div>

        {/*<div className="mb-1">
          <Row>
            <Col sm="6">
              <Label className="form-label" for="type">Users <span className="text-danger">*</span></Label>
            </Col>
            <Col sm="6" className="direction-right"><Button size={"sm"} className="mb-1" color="primary" >Select All</Button></Col>
          </Row>

          <Select isClearable={false} theme={selectThemeColors} isMulti options={membersOptions} className='react-select' classNamePrefix='select' 
          onChange={(selected) => { setMembers(selected) }} />
          {membersError && (<div className="invalid-feedback" style={{ display: "block" }}>Please select User</div>) }
        </div>*/}

        <div className="mb-1">
          <Label className="form-label" for="message">
            Message <span className="text-danger">*</span>
          </Label>

          <Input type="textarea" name="message" id="message" placeholder="Just Amazing" style={{ minHeight: '100px' }} invalid={messageError} value={message} 
            onChange={(e) => setMessage(e.target.value)} />
          <FormFeedback>Please enter a valid message</FormFeedback>
        </div>

        <Button onClick={(e) => onSubmit(e)} className="me-1 mt-1" color="primary">
          Submit {formSubmissionLoader && (
            <Spinner style={{ marginLeft: "5px" }} size={"sm"} color="white" />
          )}
        </Button>
        <Button className="mt-1" type="reset" color="secondary" outline onClick={toggleSidebar}>
          Cancel
        </Button>
      </Form>
    </Sidebar>
  );
};

export default NotificationSidebar;
