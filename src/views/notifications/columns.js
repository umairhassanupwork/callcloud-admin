// ** React Imports
import { Link } from "react-router-dom";

// ** Custom Components
import Avatar from "@components/avatar";

// ** Icons Imports
import { Slack, User, Settings, Database, Edit2, MoreVertical, FileText, Trash2, Archive } from "react-feather";

// ** Reactstrap Imports
import { Badge, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Input, Label } from "reactstrap";

const renderCreatedAt = (row) => {
  return row.createdAt ? row.createdAt : "-";
};

const Type = () => (
  <div style={{ paddingLeft: "25px" }} className="d-flex justify-content-center align-items-center">
    Type
  </div>
);

const Message = () => (
  <div style={{ paddingLeft: "55px" }} className="d-flex justify-content-center align-items-center">
    Message
  </div>
);

export const notificationTableColumns = [
  {
    name: <Type />,
    sortable: false,
    minWidth: "100px",
    selector: (row) => row.type,
    cell: (row) => (row.type === 1 ? 'Maintenance' : ''),
  },
  {
    name: <Message />,
    sortable: false,
    minWidth: "650px",
    selector: (row) => row.message,
    cell: (row) => row.message,
  },
  {
    name: "Created At",
    sortable: true,
    minWidth: "172px",
    sortField: "createdAt",
    selector: (row) => row.createdAt,
    cell: (row) => renderCreatedAt(row),
  }
];