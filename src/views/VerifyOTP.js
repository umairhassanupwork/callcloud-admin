// ** React Imports
import { useState } from "react";
import { Link, Navigate } from "react-router-dom";
import { useSkin } from "@hooks/useSkin";

// ** Reactstrap Imports
import {
  Row,
  Col,
  CardTitle,
  CardText,
  Button,
  Spinner,
  Label,
  Input,
  FormFeedback,
} from "reactstrap";

// ** Styles
import "@styles/base/pages/authentication.scss";

import { useDispatch, useSelector } from "react-redux";

// ** Config
import themeConfig from "@configs/themeConfig";

import { verifyOTP, resendOTP } from "@store/auth";

const VerifyOTP = () => {
  // ** Hooks
  const { skin } = useSkin();

  const dispatch = useDispatch();
  const [formSubmissionLoader, setFormSubmissionLoader] = useState(false);
  const [resendLoader, setResendLoader] = useState(false);
  const [otp, setOtp] = useState("");
  const [otpError, setOtpError] = useState(false);

  const store = useSelector((state) => {
    return state.auth;
  });

  //prettier-ignore
  const illustration = skin === "dark" ? "verify-email-illustration-dark.svg" : "verify-email-illustration.svg",
    source = require(`@src/assets/images/pages/${illustration}`).default;

  if (store.user && store.user.otpVerified) {
    return <Navigate to="/dashboard" />;
  }

  const handleResend = (e) => {
    setResendLoader(true);
    // prettier-ignore
    dispatch(resendOTP()).then(() => setResendLoader(false));
    e.preventDefault();
  };

  const onSubmit = (e) => {
    if (!otp || otp.length < 6) {
      setOtpError(true);
      return false;
    } else {
      setOtpError(false);
    }

    setFormSubmissionLoader(true);
    dispatch(verifyOTP({ otp })).then(() => setFormSubmissionLoader(false));
    e.preventDefault();
  };

  return (
    <div className="auth-wrapper auth-cover">
      <Row className="auth-inner m-0">
        <Link className="brand-logo" to="/" onClick={(e) => e.preventDefault()}>
          <img height={28} src={themeConfig.app.appLogoImage} alt="logo" />
          <h2 className="brand-text text-primary ms-1">CallCloud</h2>
        </Link>
        <Col className="d-none d-lg-flex align-items-center p-5" lg="8" sm="12">
          <div className="w-100 d-lg-flex align-items-center justify-content-center px-5">
            <img className="img-fluid" src={source} alt="Login Cover" />
          </div>
        </Col>
        <Col
          className="d-flex align-items-center auth-bg px-2 p-lg-5"
          lg="4"
          sm="12"
        >
          <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
            <CardTitle tag="h2" className="fw-bolder mb-1">
              Verify OTP ✉️
            </CardTitle>
            <CardText className="mb-2">
              We've sent a OTP to your email address:{" "}
              <span className="fw-bolder">{store.user.email}</span>.
            </CardText>
            {/* <Button block tag={Link} to="/" color="primary">
              Skip for now
            </Button> */}
            <p className="text-left mt-2">
              <span>Didn't receive an email? </span>
              <a href="/" onClick={(e) => handleResend(e)}>
                <span>Resend</span>

                {resendLoader && (
                  <Spinner
                    style={{ marginLeft: "5px" }}
                    size={"sm"}
                    color="white"
                  />
                )}
              </a>
            </p>

            <div>
              <Label className="form-label" for="otp">
                OTP
              </Label>
              <Input
                id="otp"
                placeholder="123456"
                invalid={otpError}
                value={otp}
                onChange={(e) => {
                  setOtp(e.target.value);
                }}
              />

              <FormFeedback>Please enter a valid OTP</FormFeedback>

              <Button
                onClick={(e) => onSubmit(e)}
                className="me-1 mt-2"
                color="primary"
              >
                Verify OTP
                {formSubmissionLoader && (
                  <Spinner
                    style={{ marginLeft: "5px" }}
                    size={"sm"}
                    color="white"
                  />
                )}
              </Button>
            </div>
          </Col>
        </Col>
      </Row>
    </div>
  );
};

export default VerifyOTP;
