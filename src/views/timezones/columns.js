// ** React Imports
import { Link } from "react-router-dom";

// ** Custom Components
import Avatar from "@components/avatar";

// ** Icons Imports
import { Slack, User, Settings, Database, Edit2, MoreVertical, FileText, Trash2, Archive } from "react-feather";

// ** Reactstrap Imports
import { Badge, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Input, Label } from "reactstrap";

const renderCreatedAt = (row) => {
  return row.createdAt ? row.createdAt : "-";
};

const Title = () => (
  <div style={{ paddingLeft: "45px" }} className="d-flex justify-content-center align-items-center">
    Name
  </div>
);

const Identifier = () => (
  <div style={{ paddingLeft: "25px" }} className="d-flex justify-content-center align-items-center">
    Identifier
  </div>
);
export const timezoneTableColumns = [
  {
    name: <Title />,
    sortable: false,
    minWidth: "250px",
    selector: (row) => row.name,
    cell: (row) => row.name,
  },
  {
    name: <Identifier />,
    sortable: false,
    minWidth: "200px",
    selector: (row) => row.identifier,
    cell: (row) => row.identifier,
  },
  {
    name: "Group",
    sortable: false,
    minWidth: "172px",
    selector: (row) => row.group,
    cell: (row) => row.group,
  },
  {
    name: "Created At",
    sortable: true,
    minWidth: "172px",
    sortField: "createdAt",
    selector: (row) => row.createdAt,
    cell: (row) => renderCreatedAt(row),
  },
  {
    name: "Actions",
    minWidth: "100px",
    cell: (row) => (
      <div className="column-action">
        <UncontrolledDropdown>
          <DropdownToggle tag="div" className="btn btn-sm">
            <MoreVertical size={14} className="cursor-pointer" />
          </DropdownToggle>
          <DropdownMenu>
             <DropdownItem tag="a" className="w-100" onClick={(e) => { row.handleEdit(row.id); e.preventDefault(); }}>
              <Archive size={14} className="me-50" />
              <span className="align-middle">Edit</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </div>
    ),
  },
];