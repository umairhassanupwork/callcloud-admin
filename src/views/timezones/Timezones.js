import Table from "./Table";

const Timezones = () => {
  return (
    <div className="app-user-list">
      <Table />
    </div>
  );
};

export default Timezones;
