import { Layers, Home, Users, Trello, DollarSign, Archive, Server, Bell, Settings } from "react-feather";

export default [
  {
    id: "dashboard",
    title: "Dashboard",
    icon: <Home size={20} />,
    navLink: "/dashboard",
  },
  // {
  //   id: "workspaces",
  //   title: "Workspaces",
  //   icon: <Layers size={20} />,
  //   navLink: "/workspaces",
  // },
  // {
  //   id: "manageUsers",
  //   title: "Users",
  //   icon: <Users size={20} />,
  //   navLink: "/users",
  // }, 
  {
    id: "companies",
    title: "Companies",
    icon: <Trello size={20} />,
    navLink: "/companies",
  },
  {
    id: "manage-plans",
    title: "Manage Plans",
    icon: <DollarSign size={20} />,
    navLink: "/manage-plans",
  },
  {
    id: "manage-timezones",
    title: "Manage Timezones",
    icon: <Layers size={20} />,
    navLink: "/manage-timezones",
  },
  {
    id: "manage-bookings",
    title: "Manage Bookings",
    icon: <Archive size={20} />,
    navLink: "/manage-bookings",
  },
  {
    id: "feedbacks",
    title: "Feedbacks",
    icon: <Server size={20} />,
    navLink: "/feedbacks",
  },
  {
    id: "site_warnings",
    title: "Site Warnings",
    icon: <Bell size={20} />,
    navLink: "/site_warnings",
  },
  {
    id: "settings",
    title: "Settings",
    icon: <Settings size={20} />,
    navLink: "/settings",
  },
];
